import './AdminCreateProduct.css'
import { useState } from "react";
import Swal from "sweetalert2";
import { useHistory } from "react-router";
import { Link } from "react-router-dom";
import UserContext from '../../UserContext.js'
import { useContext } from "react";
import { Redirect } from "react-router";
import { AdminNav } from './AdminNav';

export const AdminCreateProduct = (props) => {

    const history = useHistory()
    const {user} = useContext(UserContext);
    const [ category,setCategory] = useState('')
    const [ productName,setProductName] = useState('')
    const [ imageLink,setImageLink] = useState('')
    const [ description,setDescription] = useState('')
    const [ price,setPrice] = useState('')

    let token = localStorage.getItem('accessToken')

    function createProduct(event){
    event.preventDefault();


    
    fetch('https://murmuring-refuge-55179.herokuapp.com/products/create',{method: "POST", headers: {
      'Content-Type': 'application/json',
      'Authorization' : `Bearer ${token}`
    },
    body: JSON.stringify({
        "productName": productName,
        "url": imageLink,
        "description": description ,
        "category": category,
        "price": price
    })
  }).then(res => res.json())
    .then(data => {
     
      if (data ===true ) {

        Swal.fire({
          title:`create successfully`,
          icon: 'success',
          // text:'Welcome to Zuitt!'
        })
        console.log(data)
        history.push('/admin/dashboard');
      } else {
          alert ('Error: Create Product Unsuccessful')
      }
    })
   
  }  



  return(
    (user.isAdmin === undefined || user.isAdmin === false) 
    ? <Redirect to="/admin-login" />
    :
    <div className="container-fluid">
    <AdminNav />
    <div><h1 className="text-center">Create Product</h1></div>
    <Link to="/admin/dashboard"> <button >Back to Admin Dashboard</button> </Link>
    <div className="container createContainer">
    <form onSubmit={(event) => createProduct(event)}>

    <div>
        <label>Name:</label>
        <input type="text" value={productName} onChange={event => {setProductName(event.target.value)}} placeholder="Product Name" required />

    </div>

        <label>Image Link:</label>
        <input type="text" value={imageLink}onChange={event => {setImageLink(event.target.value)}} placeholder="Image Link/Url" required />

        <label>Description:</label>
        <textarea type="text" value={description} onChange={event => {setDescription(event.target.value)}} placeholder="Product Description" required />


        <span className="w-50">
       <label>Price:</label>
        <input className="w-50" type="number"  value={price} onChange={event => {setPrice(event.target.value)}} placeholder="Price" required />
        </span>

        <span>

        <label>Category:</label>
                <select as="select" value={category} onChange={event => {setCategory(event.currentTarget.value)}}>
                  <option disabled value="" >Select Category Here</option>
                  <option value="Shirts">Shirts</option>
                  <option value="Pants" >Pants</option>
                  <option value="Bags" >Bags</option>
                  <option value="Shoes">Shoes</option>
                </select>
        </span>

                <div className="text-center mb-3 mt-3"><button type="submit">Create Product</button></div>
        </form>
        </div>
    </div>
   )

 }