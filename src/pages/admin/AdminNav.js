import { Navbar,Nav } from "react-bootstrap";
import './AdminNav.css'


export const AdminNav = (props) => {

    const clearStorage = () => {
        window.localStorage.removeItem("accessToken");
        location.reload();
      }
  return(
    <div id="adminNav">
    <Navbar>
  <Navbar.Brand href="/admin/dashboard">Admin</Navbar.Brand>
  <Navbar.Toggle />
  <Navbar.Collapse className="justify-content-end">
    {/* <Navbar.Text>
      Signed in as: <a href="#login">Mark Otto</a>
    </Navbar.Text> */}
    <Nav.Link className="links" href="/admin-login" className="links ml-auto" onClick={clearStorage} >Logout</Nav.Link>
  </Navbar.Collapse>
</Navbar>
    </div>
   )

 }