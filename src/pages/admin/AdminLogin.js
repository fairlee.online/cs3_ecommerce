import './AdminLogin.css'
import { useHistory } from 'react-router';
// import { Link } from 'react-router-dom';
import { useState,useEffect, useContext } from 'react';
import UserContext from '../../UserContext';
import { Redirect } from "react-router";
import Swal from 'sweetalert2';


export const AdminLogin = (props) => {


  const {user, setUser} = useContext(UserContext)
  const history = useHistory();
  const [email, setEmail]=useState('');
  const [password,setPassword]=useState('');
  // const [isActive,setIsActive]=useState(false);


   const authenticateAdmin = (e) => {
		e.preventDefault()

		fetch(`https://murmuring-refuge-55179.herokuapp.com/users/login`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data.isAdmin)
      if (data.isAdmin === true) {
        // let logged =
         localStorage.setItem('accessToken',data.accessToken);
            retrieveUserDetails(data.accessToken) 
          //   if(logged !== 'undefined'){

          // history.push('/admin/dashboard')
        
        
      } else {
        Swal.fire({
          title:`Incorrect Credentials`,
          icon: 'warning',
          text: 'Incorrect Admin Email or Password'
        
        })
      }
		
		})
    
	}

  const retrieveUserDetails = (token) => {

    fetch('https://murmuring-refuge-55179.herokuapp.com/users/details',{
        headers: {
            Authorization: `Bearer ${token}`
        }
    }).then(resultOfPromise => resultOfPromise.json())
    .then(convertedResult => {
        console.log(convertedResult)
        setUser({
            id: convertedResult._id ,
            isAdmin: convertedResult.isAdmin
        });
    })
}
  console.log(user.isAdmin)

  return(
    
    (user.isAdmin === true)
    ? <Redirect to="/admin/dashboard" />  
    :
    
    <div className="container-fluid admin-bg">
       <div id="login">
     <form onSubmit={e => authenticateAdmin(e)}>


         <h1>ADMIN</h1>
            <input type="email" placeholder="Email" name="email" value={email} onChange={event => {setEmail(event.target.value)}} required/>


            <input type="password" placeholder="Password" name="password" value={password} onChange={event => {setPassword(event.target.value)}} required />
           
           
            <button type="submit" >Sign in</button>
            
    </form>
    </div>

    </div>
   
   )
  }
