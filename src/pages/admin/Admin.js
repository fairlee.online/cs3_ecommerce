import './Admin.css'
import React, { useContext, useEffect, useState } from 'react'
import UserContext from '../../UserContext.js'
import { Redirect } from "react-router";
import { Link } from 'react-router-dom';
import { Row } from 'react-bootstrap';
import { AdminNav } from './AdminNav';


export const  AdminDashboard = () => {

  const {user} = useContext(UserContext);
  const [products, setProducts] = useState([])
  const [productId, setProductId] = useState('');
  const [url, setUrl] = useState('');
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState('');
  const [category, setCategory] = useState('');
  const [status, setStatus] = useState('');
  // const [isActive, setActive] = useState(status);



  useEffect(() => {
    getItems();
  }, [])
  function getItems() {
    fetch("https://murmuring-refuge-55179.herokuapp.com/products/admin/product-list").then((result) => {
      result.json().then((resp) => {
        // console.warn(resp)
        setProducts(resp)
        setProductId(resp[0]._id)
        setUrl(resp[0].url)
        setName(resp[0].name)
        setDescription(resp[0].description)
        setPrice(resp[0].price)
        setCategory(resp[0].category)
        setStatus(resp[0].isActive)
        
      
      })
    })
  }

  function deleteProduct(id) {

    let token = localStorage.getItem('accessToken');

    fetch(`https://murmuring-refuge-55179.herokuapp.com/products/archive`, {
      method: 'PUT',
      headers:{
        'Accept':'application/json',
        'Content-Type':'application/json',
        'Authorization': `Bearer ${token} `
      },
      body:JSON.stringify({
        "id": id
        
      })
    }).then((result) => {
      result.json().then((resp) => {
        console.warn(resp)
        getItems()
      })
    })
  }

  function reactivateProduct(id) {

    let token = localStorage.getItem('accessToken');

    fetch(`https://murmuring-refuge-55179.herokuapp.com/products/reactivate`, {
      method: 'PUT',
      headers:{
        'Accept':'application/json',
        'Content-Type':'application/json',
        'Authorization': `Bearer ${token} `
      },
      body:JSON.stringify({
        "id": id
        
      })
    }).then((result) => {
      result.json().then((resp) => {
        console.warn(resp)
        getItems()
      })
    })
  }

  
  function selectProduct(id)
  {
        // let result = products.filter(e => e._id === id)
         
        const found = products.find(element => element._id === id);
      
            setProductId(found._id)
            setName(found.name)
            setUrl(found.url)
            setDescription(found.description)
            setCategory(found.category)
            setPrice(found.price) 
          
            console.log(found._id)
  }
  
  function updateProduct()
  {
    // let item={productId, name,category,description,price,category}
    // console.warn("item",item)
    let token = localStorage.getItem('accessToken');

    fetch(`https://murmuring-refuge-55179.herokuapp.com/products/update`, {
      method: 'PUT',
      headers:{
        'Accept':'application/json',
        'Content-Type':'application/json',
        'Authorization': `Bearer ${token} `
      },
      body:JSON.stringify({
        "id":productId,
        "name": name ,
        "url": url,
        "description": description ,
        "category":category,
        "price": price
      })
    }).then((result) => {
      result.json().then((resp) => {
        console.warn(resp)
        getItems()
      })
    })
  } 





  return (
    (user.isAdmin === undefined || user.isAdmin === false) 
    ? <Redirect to="/admin-login" />
    :
    <div className="App">
      <AdminNav/>
      <h1 className="text-center">ADMIN DASHBOARD</h1>
      <div className="container text-right mb-3">
       <Link to="/admin/create">
        <button>Create Product</button>
       </Link>  </div>

      <Row>
      <div className="container" id="productList">
      <table  border="1" style={{ float: 'left' }}>
        <tbody>
          <tr>
            <td>ID</td>
            <td>Image Link</td>
            <td>Product Name</td>
            <td>Description</td>
            <td>Price</td>
            <td>Category</td>
            <td>Status</td>
          </tr>
          {
            products.map((item, i) =>
              <tr key={i}>
                <td className="tableDetails">{item._id}</td>
                <td className="tableDetails"><img src={item.url} alt="unavailable"/></td>
                <td className="tableDetails">{item.name}</td>
                <td className="tableDetails">{item.description}</td>
                <td className="tableDetails">{item.price}</td>
                <td className="tableDetails">{item.category}</td>
                <td className="tableDetails">{item.isActive.toString()}</td>

                <td className="adminBtn">
                {(item.isActive === false)
                ? 
                <button onClick={() => reactivateProduct(item._id)}>Reactivate  </button>
                :
                <button onClick={() => deleteProduct(item._id)}>Deactivate</button>
                }


               <button onClick={() => selectProduct(item._id)}>Update Info</button>
               </td>
              </tr>
            )
          }
        </tbody>
      </table>
      </div>
      </Row>
      <Row className=" ml-auto mr-auto mt-3">
      <div className="container updateInfoBar">
      
         <label>Image Link:</label>
        <input type="text" value={url} onChange={(e)=>{setUrl(e.target.value)}} />

        <label>Product Name:</label>
        <input type="text" value={name} onChange={(e)=>{setName(e.target.value)}} /> 

        <label>description:</label>
        <input type="text" value={description} onChange={(e)=>{setDescription(e.target.value)}} /> 

        <label>Price:</label>
        <input type="number" value={price}  onChange={(e)=>{setPrice(e.target.value)}} /> 

        <label>Category:</label>
          <select as="select" value={category} onChange={event => {setCategory(event.currentTarget.value)}}>
                    <option selected disabled value='' >Select Category Here</option>
                    <option value="Shirts">Shirts</option>
                    <option value="Pants" >Pants</option>
                    <option value="Bags" >Bags</option>
                    <option value="Shoes">Shoes</option>
                  </select>
        <div className="float-right"><button onClick={updateProduct} >Update Product</button> </div>
      </div>
      </Row>
    </div>
  );
}