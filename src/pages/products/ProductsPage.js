import "./ProductsPage.css";
import { Row, Col } from "react-bootstrap";
import { ProductsList } from "./Products";
import { useEffect,useState } from "react";
import { useParams } from "react-router";
// import logo from '/TD-retro.png'

export const GetProducts = () => {
    
  
  const [products, setProducts] = useState([]);
  const [search, setSearch] = useState('')
  // const [filtered,setFiltered] = useState([]);


  const link = 'https://murmuring-refuge-55179.herokuapp.com/products/products-list';

  useEffect(() => {
     
     fetch(link).then(outcomeNgFetch => outcomeNgFetch.json()).then(convertedData => {
      // console.log(convertedData)
          //the map method will loop through each individual course object inside our array and tweak then 1 by 1
          setProducts(convertedData )
})},[link])

let filtered = products.filter( items => {
  return (Object.values(items).join(" ").toLowerCase().includes( search.toLowerCase()))
})

let searchedProducts = filtered.map((product) => {
  return(<ProductsList key={product._id} propsProducts={product}/>)})




  return (
    <div id="products-bg">
      <div className="container ">
      
                <div id="searchBarProducts" >
                   <input type="text" placeholder="Search..." id="searchProducts" value={search} onChange={e =>setSearch(e.target.value)} />
                </div>

        <Row id="promotionBar">
          <img src='/TD-retro.png' alt="logo" className="ml-auto mr-auto mb-2"></img>
        </Row>
          
        <div id="productSection">
     
          {searchedProducts}
          
        </div>
      </div>
    </div>
  );
};
