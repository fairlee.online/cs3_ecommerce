import './Products.css'
import { Link } from 'react-router-dom'

export const ProductsList = ({propsProducts}) => {

  const { _id, name,url ,price} = propsProducts
  return(
    <div className="container productBox text-center"  >
      <div >
      
            <div  id="productArea">
                <img src={url} alt="unavailable" className="productImage img-fluid" />
                    <div>
                    Name: {name} </div> 
                    <div className="p-1">price: {price} PHP</div> 


            <div className="mb-4 p-2">
                <button className="mr-2 mb-1 buttonProduct">add to cart</button> 
                <Link to={`/item/${_id}`}>
                <button className="buttonProduct" type="button">details</button>    
                </Link>
            </div>

            </div>
      </div>
    </div>
   )
  }
