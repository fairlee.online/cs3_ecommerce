import { useState, useEffect } from "react";
import React from "react";
import "./Register.css";
import register from '../../images/register.png';
import Swal from "sweetalert2";
import { useHistory } from "react-router";





export const Register = () => {

  const history = useHistory();
  const [firstName, setFirstName]  = useState('');
  const [middleName, setMiddleName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [password2, setPassword2] = useState('');
  const [mobileNumber, setMobileNumber] = useState('');

  //fetching for registration
  function registerUser(event){
    event.preventDefault();

    
    console.log(firstName)
    console.log(middleName)
    console.log(lastName)
    console.log(email)
    console.log(password)
    console.log(password2)
    console.log(mobileNumber)
    
    fetch('https://murmuring-refuge-55179.herokuapp.com/users/register/',{method: "POST", headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      firstName: firstName ,
      lastName: lastName,
      middleName: middleName,
      email:  email,
      password: password,
      mobileNumber: mobileNumber 
    })
  }).then(res => res.json())
    .then(data => {
     
      if (data === true) {

        Swal.fire({
          title:`Hey, your account is registered successfully`,
          icon: 'success',
          // text:'Welcome to Zuitt!'
        })
        history.push('/login');
      }
    })
    //first .then() is the raw data being processed.
    //second .then() we receive the information(parsed data)//return result

  }  



  //USEFFECT
  const [isPasswordMatch, setPasswordMatch] = useState(false);
  const [isPasswordValid,setPasswordValid] = useState(false)

   //password confirmation
  useEffect(()=>{
    if(password.length > 7 && password.length < 17) {
        setPasswordValid(true)
    } else {
      setPasswordValid(false)
    }
  })

   useEffect(() => {
    if ( password !== '' && password.length > 7 && password === password2){
      setPasswordMatch(true)
    } else {
      setPasswordMatch(false)
    }
  },[password, password2])








  return(
   <div id="register">
      <div className="container" >
         <div id="registerBox">
          <form onSubmit={(event) => registerUser(event)} id="registerInput">


            <img src={register} alt="not available" className="registerLogo img-fluid"/>

         
            
            <label >First Name:</label>
            <input type="text" name="firstName" placeholder="First Name"  onChange={event => {
              setFirstName(event.target.value)}} value={firstName} required />
           

            <label >Middle Name:</label>
            <input type="text" name="middleName" placeholder="Middle Name"  required onChange={event => {
              setMiddleName(event.target.value)}} value={middleName}  />

            <label >Last Name:</label>
            <input type="text" name="lastName" placeholder="Last Name"  onChange={event => {
              setLastName(event.target.value)}} value={lastName} required />

            <label >Email:</label>
            <input type="email" name="email" placeholder="Email"  onChange={event => {
              setEmail(event.target.value)}} value={email} required />

            <label >Password:
            {
              isPasswordValid

                  ? <span className="text-success"> Password Valid</span>  
                  : <span className="text-muted"> Please enter 8-16 characters</span> 
            }
            
            </label>
            <input type="password" name="password" placeholder="Password"  onChange={event => {
              setPassword(event.target.value)}} value={password} required  />


            <label> Confirm Password:

                {/* password useEffect */}

                {
                  (password.length < 7 && password2.length < 7) ? <span></span> :
                    
                  isPasswordMatch
                  ?
                  <span className="text-success"> *Password Matched!*</span>
                  :
                  <span className="text-danger"> *Password should Match.*</span>
                }
            </label>
              
           
            <input type="password" name="password2" placeholder="Confirm Password"  onChange={event => {
              setPassword2(event.target.value)}} value={password2} required />

            <label > Mobile Number:</label>
            <input type="number" name="mobileNumber" placeholder="Mobile Number"  onInput={(e) => (e.target.value = e.target.value.slice(0, 11))} onChange={event => {setMobileNumber(event.target.value)}} value={mobileNumber} required />

         
           

            <button  type="submit" id="registerBtn"> Register</button>

          </form>
      </div>
    </div>
   </div>
   
   )

 }