import { Col, Row } from 'react-bootstrap'
import './Login.css'
import photo from '../../images/login.png'
import { useState, useEffect } from 'react'
import { useHistory } from "react-router";
import Swal from 'sweetalert2';


export const Login = (props) => {

  const history = useHistory();
  const [email, setEmail]=useState('');
  const [password,setPassword]=useState('');
  // const [isActive,setIsActive]=useState(false);


   const authenticate = (e) => {
		e.preventDefault()

		fetch(`https://murmuring-refuge-55179.herokuapp.com/users/login`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if (data === false) {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Incorrect Email or Password',
          showConfirmButton: true,
          showCloseButton: true
        })
        // window.location.reload();
      } else {

        localStorage.setItem('accessToken', data.accessToken);
        history.push('/products');
      }
      window.location.reload();
		})
    
	}




  return(
    <div id="login-bg">
      <div  className="container-fluid" >
        <Row id="loginContainer">
        {/* only image side */}
        <Col id="posterLogin">
        <img src="https://www.designshock.com/wp-content/uploads/2017/01/195336311.jpg" alt="not available" className="img-fluid"/>
        </Col>

        {/* signin side */}
        <Col id="LoginSide">
          <div className="text-center">
            <img src={photo} alt="none" id="loginPhoto" /> 
          </div>
             <form onSubmit={e => authenticate(e)} id="loginInput">

            <div>
              <label >Email:</label>

            </div>
            <input type="email" placeholder="Email" name="email" value={email} onChange={event => {setEmail(event.target.value)}} required />

            <div>

            <label >Password:</label>
            </div>
            <input type="password" placeholder="Password" name="password" value={password} onChange={event => {setPassword(event.target.value)}} required />



            <div className="text-center mt-3"> 
            <button type="submit" id="loginBtn" >send</button>
            </div>

            </form>
          <p className="text-center mt-2">are you an ADMIN?<a href='/admin-login'>Login Here</a></p>
        </Col>
          </Row>
      </div>
    </div>
   )

 }