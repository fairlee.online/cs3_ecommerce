import { Col, Row } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import './SingleProduct.css'
import { useParams } from 'react-router'
import { useEffect, useState } from 'react'
import backImg from '../../images/back.png'

export const Item = () => {
    const params = useParams();
    console.log(params.id)

    const [singleProduct, setSingleProduct] = useState([]);

    useEffect(() => {
       
       fetch(`https://murmuring-refuge-55179.herokuapp.com/products/info/${params.id}`).then(outcomeNgFetch => outcomeNgFetch.json()).then(convertedData => {
            setSingleProduct(convertedData)
            console.log(convertedData)
       }

  )},[params])


console.log(singleProduct.name)
  

  return(
    <div className="container-fluid" id="item-bg">

        <Link to="/products"><img src={backImg} alt ="back Image" width="500" className="mt-4" /></Link>

        <div className="container" id="itemBox">
            <Row>
                <Col>
                <img src={singleProduct.url} alt="unavailable" className="itemImage img-fluid" />
                   
                </Col>
                <Col>
                    <h5 className="mt-5 productInfo">Name:</h5> 
                    <h1>{singleProduct.name}</h1>
                    
                    <h5 className="productInfo">Description:</h5>
                    <h2>{singleProduct.description}</h2>

                    <h6 className="productInfo">Price:</h6>
                    <h2>PHP {singleProduct.price}</h2>

                    <h6 className="productInfo">Category:</h6>
                    <h3>{singleProduct.category}</h3>


                    <button>Add to Cart</button>
                    <button>Checkout</button>


                </Col>
            </Row>
        </div>
    </div>
   )
  }
