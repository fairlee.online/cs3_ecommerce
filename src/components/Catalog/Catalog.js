import React from 'react'
import { Link } from 'react-router-dom'
import './Catalog.css'
import shopNow from '../../images/shopnow.png'
import 'animate.css';



export const MainCategories = () => {

  return(
    <React.Fragment>
        <div id="mainCategories">
            <div id="shirts">
                <img src="/shirts-td.png" alt="shirts" />
            </div>
            <div id="pants">
                <img src="/pants-td.png" alt="shirts"></img>
            </div>
            <div id="shoes">
                <img src="/shoes-td.png" alt="shirts"></img>
            </div>
            <div id="bags"> 
                <img src="/bags-td.png" alt="shirts"></img>
            </div>

        <div id="shopNow-img">
            <Link to="/products">  <img className="text-center img-fluid ml-auto mr-auto animate__animated animate__tada" src={shopNow} alt="unavailable" /></Link>

        </div>

        </div>
         
    </React.Fragment>
   )

 }            