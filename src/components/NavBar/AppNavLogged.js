import React from 'react'
import { Nav, Navbar} from 'react-bootstrap';
import './AppNav.css'


export const AppNavBarLogged = () => {

  const clearStorage = () => {
    window.localStorage.removeItem("accessToken");
    location.reload();
  }
  
  return(
    <>
    <Navbar  expand="lg" id="navbar" >
      <Navbar.Toggle aria-controls="basic-navbar" />
      <Navbar.Collapse id="basic-navbar">
            <Nav.Link className="links" href="/" >Home</Nav.Link>
            <Nav.Link className="links" href="/products" >Products</Nav.Link>
            <Nav.Link className="links" href="/login" className="links ml-auto" onClick={clearStorage} >Logout</Nav.Link>
            {/* <Nav.Link  href="/login" className="links ml-auto">Login</Nav.Link>
            <Nav.Link className="links" href="/register" >Register</Nav.Link> */}
            
  </Navbar.Collapse>
</Navbar>

    </>
    
   )

 }