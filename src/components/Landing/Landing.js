import { Carousel, Col, Container, Row } from "react-bootstrap"
import './Landing.css'
import { MainCategories } from "../Catalog/Catalog"
import React from "react"
import carousel1 from '../../images/vintage-sale-sign-1626398.png'
import carousel2 from '../../images/carousel2.png'
import carousel3 from '../../images/carousel3.png'

export const Landing = () => {
  return(
    <React.Fragment>
    <div id="landingPage" className="container-fluid">
    <Container className="d-flex text-center">
        <Row id="rowContainer1">
            {/* logo */}
            <Col className="" >
                <img src="/TD-retro.png" alt="Logo Unavailable" id="shopLogo" className="img-fluid"></img>
            </Col>

        <Col >

    
        <Carousel id="carouselPromotion">
        <Carousel.Item interval={1000}>
            <img
            className="img-fluid"
            src={carousel3}
            alt="First slide"/>
        </Carousel.Item>
        <Carousel.Item interval={500}>
            <img
            className="img-fluid"
            src={carousel1}
            alt="Second slide"
            />
        </Carousel.Item>
        <Carousel.Item>
            <img
            className="img-fluid"
            src={carousel2}
            alt="Third slide"
            />
        </Carousel.Item>
        </Carousel>
        </Col>
        </Row>

    </Container>

    <MainCategories/>

    </div>
    </React.Fragment>
   )

 }