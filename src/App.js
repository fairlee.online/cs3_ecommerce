import React from 'react';
import { useEffect, useState } from 'react';
import './App.css';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { AppNavBar } from './components/NavBar/AppNav';
import { Landing } from './components/Landing/Landing';
import { Footer } from './components/Footer/Footer';
import { Login } from './pages/login/Login';
import { Register } from './pages/register/Register';
import {GetProducts} from './pages/products/ProductsPage'
import  {Item } from './pages/singleProduct/SingleProduct';
import {AdminDashboard} from './pages/admin/Admin'
import { AdminLogin } from './pages/admin/AdminLogin';
import { AppNavBarLogged } from './components/NavBar/AppNavLogged';
import {UserProvider} from './UserContext';
import { AdminCreateProduct } from './pages/admin/AdminCreateproduct';


function App() {

  const [isUserLoggedIn , setLogged ] = useState (false)



 useEffect(()=> {
  
    let loggedIn = localStorage.getItem('accessToken')
  
    if(loggedIn !== undefined && loggedIn !== null){
      
      setLogged(true)
    } else{
      setLogged(false)
    }

  },[localStorage])

  const [user,setUser] = useState({
    id:null,
    isAdmin:null
  });

  const unsetUser = () => {
    localStorage.clear();
    setUser({
      id:null,
      isAdmin:null
    })
  }

  useEffect(()=>{
    fetch('https://murmuring-refuge-55179.herokuapp.com/users/details',{
      headers: {
          Authorization: `Bearer ${localStorage.getItem('accessToken')}`
      }
  }).then(resultOfPromise => resultOfPromise.json())
  .then(convertedResult => {
      console.log(convertedResult)
     if (convertedResult._id !== "undefined"){

      setUser({
        id:convertedResult._id,
        isAdmin: convertedResult.isAdmin
      })
     }else{
      setUser({
        id:null,
        isAdmin:null
      })
     }
  })
  }, [])  


 
  return (
    <UserProvider value={{user, unsetUser, setUser}}>
      <Router>
      
          <Switch>
            <Route exact path="/" component={Landing} > 
            {
              isUserLoggedIn ? <AppNavBarLogged/> :  <AppNavBar />
            }
            <Landing/> </Route>
            <Route exact path="/login" component={Login}> {
              isUserLoggedIn ? <AppNavBarLogged/> :  <AppNavBar />
            }<Login/></Route> 
            <Route exact path="/register" component={Register}>  {
              isUserLoggedIn ? <AppNavBarLogged/> :  <AppNavBar />
            }<Register/></Route>  
            <Route exact path="/products" component={GetProducts}>  {
              isUserLoggedIn ? <AppNavBarLogged/> :  <AppNavBar />
            }<GetProducts/></Route> 
            <Route exact path="/item/:id" component={Item} >  {
              isUserLoggedIn ? <AppNavBarLogged/> :  <AppNavBar />
            }<Item/></Route> 
            <Route exact path="/admin-login" component={AdminLogin} >  <AppNavBar /><AdminLogin/></Route>
            <Route exact path="/admin/dashboard" component={AdminDashboard} />
            <Route exact path="/admin/create" component={AdminCreateProduct} />

            
          </Switch>
      <Footer/>
      </Router>
    </UserProvider>
  )
}

export default App;
